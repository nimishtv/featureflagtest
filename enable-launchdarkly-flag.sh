#!/bin/bash -e

# LAUNCHDARKLY_ACCESS_TOKEN         (Required) API Access token for your LaunchDarkly account (https://app.launchdarkly.com/settings#/tokens) 
# LAUNCHDARKLY_PROJECT_KEY          (Required) LaunchDarkly project key
# LAUNCHDARKLY_ENVIRONMENT_KEY      (Required) LaunchDarkly environment key
# LAUNCHDARKLY_FLAG_KEY             (Required) LaunchDarkly flag key

curl --fail \
    --request PATCH \
    -H "Content-type: application/json" \
    -H "Authorization: ${LAUNCHDARKLY_ACCESS_TOKEN}" \
    --data "[{\"op\": \"replace\", \"path\": \"/environments/${LAUNCHDARKLY_ENVIRONMENT_KEY}/on\", \"value\":true}]" \
    https://app.launchdarkly.com/api/v2/flags/${LAUNCHDARKLY_PROJECT_KEY}/${LAUNCHDARKLY_FLAG_KEY}